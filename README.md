# zb-restful-api

#### 介绍
封装zb常用的行情接口,交易接口

#### 软件架构

springboot2.x + okhttp3 + Retrofit2 + hutool工具类,提供springboot-starter引入方式
难点: zb交易所返回json格式不一致无法统一对象化处理,只能统一容错处理,将交易环节集中在过滤器统一排序,加密,验签

#### 安装教程

1.  引入依赖

```xml
<dependency>
	<groupId>cc.chenwenxi</groupId>
	<artifactId>zb-restful-api</artifactId>
	<version>0.0.1-SNAPSHOT</version>
</dependency>
```

2. application.yml 加入配置

```yml
npc:
  exchange:
    zb:
      market-url: http://api.zb.live/data/v1/
      trade-url: https://trade.zb.live/api/
      access-key: 
      secret-key: 
```

#### 使用说明

1.  在代码中测试

```java
private @Autowired MarketApiRestService marketApi;
private @Autowired TradeApiRestService tradeApi;

private Symbol symbol = new Symbol("ltc", Quote.qc);

@Test
public void account() {
	AccountBody account = tradeApi.getAccount();
	System.out.println(account);
}

@Test
public void getDepth() {
	DeptResult depth = marketApi.getDepth(symbol, 5);
	System.out.println(depth);
}
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
