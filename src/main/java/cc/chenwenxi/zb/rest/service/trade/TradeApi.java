package cc.chenwenxi.zb.rest.service.trade;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface TradeApi {

	@GET("getAccountInfo")
	Call<ResponseBody> getAccount(@Query("method") String method);

	@GET("order")
	Call<ResponseBody> trade(@Query("method") String method, @Query("currency") String currency,
			@Query("price") String price, @Query("amount") String amount, @Query("tradeType") Integer tradeType);
	
	@GET("orderMoreV2")
	Call<ResponseBody> tradeMoreV2(@Query("method") String method, @Query("market") String market,
			@Query("tradeParams") String tradeParams, @Query("tradeType") Integer tradeType);

	@GET("getOrder")
	Call<ResponseBody> getrder(@Query("method") String method, @Query("currency") String currency,
			@Query("id") String orderId);

	@GET("getUnfinishedOrdersIgnoreTradeType")
	Call<ResponseBody> getUnfinishedOrdersIgnoreTradeType(@Query("method") String method,
			@Query("currency") String currency, @Query("pageIndex") Integer pageIndex,
			@Query("pageSize") Integer pageSize);

	@GET("cancelOrder")
	Call<ResponseBody> cancelOrder(@Query("method") String method, @Query("currency") String currency,
			@Query("id") String orderId);
}
