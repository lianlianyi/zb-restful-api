package cc.chenwenxi.zb.rest.service.trade;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import cc.chenwenxi.zb.rest.service.market.MarketApi;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.gson.JsonObject;

import cc.chenwenxi.papaya.entity.exchange.Symbol;
import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import cc.chenwenxi.zb.rest.domain.reqs.TradeMoreBody;
import cc.chenwenxi.zb.rest.domain.resp.AccountBody;
import cc.chenwenxi.zb.rest.domain.resp.OrderBody;
import cc.chenwenxi.zb.rest.domain.resp.TradeBody;
import cc.chenwenxi.zb.rest.service.ApiClient;
import melody.base.entity.type.SideEnum;

@Component
public class TradeApiRestServiceImpl implements TradeApiRestService {
//	private TradeApi service;

//	public TradeApiRestServiceImpl(ZbApiConfig config) {
//		service = new ApiClient(config, config.getTradeUrl()).createService(TradeApi.class);
//	}

	@Override
	public TradeBody trade(ZbApiConfig config,Symbol symbol, BigDecimal price, BigDecimal amount, SideEnum tradeType) {
		TradeApi service = this.config(config);
		Integer side = tradeType == SideEnum.buy ? 1 : 0;
		return ApiClient.executeSync(service.trade("order", this.getInstrumentId(symbol), price.toPlainString(),
				amount.toPlainString(), side), TradeBody.class);
	}

	@Override
	public TradeBody tradeMoreV2(ZbApiConfig config,Symbol symbol, List<TradeMoreBody> tradeParams, SideEnum tradeType) {
		TradeApi service = this.config(config);
		Integer side = tradeType == SideEnum.buy ? 1 : 0;
		String tradeParamsStr = JSONObject.toJSONString(tradeParams.stream().map(tp -> {
			return new Object[] { tp.getPrice(), tp.getAmount() };
		}).collect(Collectors.toList()).toArray());
		return ApiClient.executeSync(
				service.tradeMoreV2("orderMoreV2", this.getInstrumentId(symbol), tradeParamsStr, side),
				TradeBody.class);
	}

	@Override
	public AccountBody getAccount(ZbApiConfig config) {
		TradeApi service = this.config(config);
		return ApiClient.executeSync(service.getAccount("getAccountInfo"), AccountBody.class);
	}

	@Override
	public OrderBody getOrder(ZbApiConfig config,Symbol symbol, String orderId) {
		TradeApi service = this.config(config);
		return ApiClient.executeSync(service.getrder("getOrder", this.getInstrumentId(symbol), orderId),
				OrderBody.class);
	}

	@Override
	public List<OrderBody> getUnfinishedOrdersIgnoreTradeType(ZbApiConfig config,Symbol symbol, Integer pageIndex, Integer pageSize) {
		TradeApi service = this.config(config);
		List<OrderBody> temp = ApiClient.executeSync(service.getUnfinishedOrdersIgnoreTradeType("getUnfinishedOrdersIgnoreTradeType",
				this.getInstrumentId(symbol), pageIndex, pageSize), List.class);
		String jsonString = JSONObject.toJSONString(temp);
		try {
			return JSONObject.parseArray(jsonString, OrderBody.class);
		}catch(Exception e) {

		}
		return Lists.newArrayList();
	}

	@Override
	public TradeBody cancelOrder(ZbApiConfig config,Symbol symbol, String orderId) {
		TradeApi service = this.config(config);
		return ApiClient.executeSync(service.cancelOrder("cancelOrder", this.getInstrumentId(symbol), orderId),
				TradeBody.class);
	}

	@Override
	public TradeBody cancelOrder(ZbApiConfig config,String instrumentId, String orderId) {
		TradeApi service = this.config(config);
		return ApiClient.executeSync(service.cancelOrder("cancelOrder", instrumentId, orderId),
				TradeBody.class);
	}
}
