package cc.chenwenxi.zb.rest.service.market;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MarketApi {

    @GET("depth")
    Call<ResponseBody> getDepth(@Query("market") String symbol,@Query("size") Integer size);
    
    @GET("kline")
    Call<ResponseBody> getKline(@Query("market") String symbol,@Query("type") String type,@Query("size") Integer size);

    @GET("ticker")
    Call<ResponseBody> getTicker(@Query("market") String symbol);
    
    @GET("allTicker")
    Call<ResponseBody> getTickers();
    
    @GET("markets")
    Call<ResponseBody> getMarkets();
    
    
}
