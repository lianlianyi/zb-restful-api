package cc.chenwenxi.zb.rest.service.market;

import java.util.List;

import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import cc.chenwenxi.zb.rest.service.ApiClient;
import com.okcoin.commons.okex.open.api.bean.spot.result.Product;

import cc.chenwenxi.papaya.entity.exchange.Symbol;
import cc.chenwenxi.zb.rest.domain.resp.TickerBody;
import cc.chenwenxi.zb.rest.domain.resp.TickerBody.Ticker;
import cn.hutool.core.text.StrFormatter;
import melody.base.entity.res.DeptResult;
import melody.base.entity.res.KlineResult;
import melody.base.entity.type.KTimeEnum;

public interface MarketApiRestService {
	DeptResult getDepth(ZbApiConfig config,Symbol symbol, Integer size);

	List<KlineResult> getKline(ZbApiConfig config,Symbol symbol, KTimeEnum ktime, Integer size);

	List<KlineResult> getKline(ZbApiConfig config,String instrument_id, KTimeEnum ktime, Integer size);

	TickerBody getTicker(ZbApiConfig config,Symbol symbol);

	List<Ticker> getTickers(ZbApiConfig config);

	List<Product> getMarkets(ZbApiConfig config);

	default String getInstrumentId(Symbol symbol) {
		return StrFormatter.format("{}_{}", symbol.getCoin().toLowerCase(), symbol.getQuote().name());
	}

	default MarketApi config(ZbApiConfig config){
		return new ApiClient(config,config.getMarketUrl()).createService(MarketApi.class);
	}

}
