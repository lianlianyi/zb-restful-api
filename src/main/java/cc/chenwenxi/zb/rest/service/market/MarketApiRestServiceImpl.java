package cc.chenwenxi.zb.rest.service.market;

import java.util.List;

import org.springframework.stereotype.Component;

import com.okcoin.commons.okex.open.api.bean.spot.result.Product;

import cc.chenwenxi.papaya.entity.exchange.Symbol;
import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import cc.chenwenxi.zb.rest.domain.resp.TickerBody;
import cc.chenwenxi.zb.rest.domain.resp.TickerBody.Ticker;
import cc.chenwenxi.zb.rest.service.ApiClient;
import melody.base.entity.res.DeptResult;
import melody.base.entity.res.KlineResult;
import melody.base.entity.type.KTimeEnum;

@Component
public class MarketApiRestServiceImpl implements MarketApiRestService {

//    private MarketApi service;

//    public MarketApiRestServiceImpl(ZbApiConfig config) {
//        service = new ApiClient(config,config.getMarketUrl()).createService(MarketApi.class);
//    }

    @Override
    public DeptResult getDepth(ZbApiConfig config,Symbol symbol,Integer size) {
		MarketApi service = this.config(config);
    	DeptResult executeSync = ApiClient.executeSync(service.getDepth(this.getInstrumentId(symbol),size),DeptResult.class);
        return executeSync;
    }


	@Override
	public List<KlineResult> getKline(ZbApiConfig config,Symbol symbol, KTimeEnum ktime, Integer size) {
		MarketApi service = this.config(config);
		return ApiClient.executeSync(service.getKline(this.getInstrumentId(symbol), ktime.getZbKey(), size), List.class);
	}

	@Override
	public TickerBody getTicker(ZbApiConfig config,Symbol symbol) {
		MarketApi service = this.config(config);
		return ApiClient.executeSync(service.getTicker(this.getInstrumentId(symbol)), TickerBody.class);
	}

	@Override
	public List<KlineResult> getKline(ZbApiConfig config,String instrument_id, KTimeEnum ktime, Integer size) {
		MarketApi service = this.config(config);
		List<KlineResult> executeSync = ApiClient.executeSync(service.getKline(instrument_id, ktime.getZbKey(), size), List.class);
		executeSync.forEach(k->{
			k.setZhouqi(ktime);
		});
		return executeSync;
	}

	@Override
	public List<Ticker> getTickers(ZbApiConfig config) {
		MarketApi service = this.config(config);
		return ApiClient.executeSync(service.getTickers(),List.class);
	}

	@Override
	public List<Product> getMarkets(ZbApiConfig config) {
		MarketApi service = this.config(config);
		return ApiClient.executeSync(service.getMarkets(),List.class);
	}
}
