package cc.chenwenxi.zb.rest.service.trade;

import java.math.BigDecimal;
import java.util.List;

import cc.chenwenxi.papaya.entity.exchange.Symbol;
import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import cc.chenwenxi.zb.rest.domain.reqs.TradeMoreBody;
import cc.chenwenxi.zb.rest.domain.resp.AccountBody;
import cc.chenwenxi.zb.rest.domain.resp.OrderBody;
import cc.chenwenxi.zb.rest.domain.resp.TradeBody;
import cc.chenwenxi.zb.rest.service.ApiClient;
import cc.chenwenxi.zb.rest.service.market.MarketApi;
import cn.hutool.core.text.StrFormatter;
import melody.base.entity.type.SideEnum;

public interface TradeApiRestService {
	default String getInstrumentId(Symbol symbol) {
		return StrFormatter.format("{}_{}", symbol.getCoin().toLowerCase(), symbol.getQuote().name());
	}

	default TradeApi config(ZbApiConfig config){
		return new ApiClient(config,config.getTradeUrl()).createService(TradeApi.class);
	}

	AccountBody getAccount(ZbApiConfig config);

	TradeBody trade(ZbApiConfig config,Symbol symbol, BigDecimal price, BigDecimal amount, SideEnum tradeType);

	TradeBody tradeMoreV2(ZbApiConfig config,Symbol symbol, List<TradeMoreBody> tradeParams, SideEnum tradeType);

	OrderBody getOrder(ZbApiConfig config,Symbol symbol, String orderId);

	List<OrderBody> getUnfinishedOrdersIgnoreTradeType(ZbApiConfig config,Symbol symbol, Integer pageIndex, Integer pageSize);

	TradeBody cancelOrder(ZbApiConfig config,Symbol symbol, String orderId);

	TradeBody cancelOrder(ZbApiConfig config,String instrumentId, String orderId);

}
