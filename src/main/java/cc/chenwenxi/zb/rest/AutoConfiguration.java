package cc.chenwenxi.zb.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import cc.chenwenxi.zb.rest.service.market.MarketApiRestService;
import cc.chenwenxi.zb.rest.service.market.MarketApiRestServiceImpl;
import cc.chenwenxi.zb.rest.service.trade.TradeApiRestService;
import cc.chenwenxi.zb.rest.service.trade.TradeApiRestServiceImpl;


@Configuration(value = "ZbAutoConfiguration")
public class AutoConfiguration {
	private @Autowired ZbApiConfig config;

//	@Bean
//	@ConditionalOnMissingBean
//	public MarketApiRestService MarketApiRestServiceImpl() {
//		return new MarketApiRestServiceImpl(config);
//	}

//	@Bean
//	@ConditionalOnMissingBean
//	public TradeApiRestService TradeApiRestServiceImpl() {
//		System.out.println(config);
//		return new TradeApiRestServiceImpl(config);
//	}
}
