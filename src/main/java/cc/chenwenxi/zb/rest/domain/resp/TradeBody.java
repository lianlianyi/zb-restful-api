package cc.chenwenxi.zb.rest.domain.resp;

import lombok.Data;

@Data
public class TradeBody {
	private long code;
	private String message;
	private String id;//订单id
	private Object data;
}
