package cc.chenwenxi.zb.rest.domain.resp;

import java.math.BigDecimal;
import java.util.List;

import lombok.Data;

@Data
public class AccountBody {
	private Result result;
	private boolean leverPerm;
	private boolean otcPerm;
	private boolean assetPerm;
	private boolean moneyPerm;
	private boolean subUserPerm;
	private boolean entrustPerm;

	@Data
	public static class Result {
		private List<Coin> coins;
		private Base base;

		@Data
		public static class Coin {
			private boolean isCanWithdraw;
			private boolean canLoan;
			private int fundstype;
			private String showName;
			private boolean isCanRecharge;
			private String cnName;
			private String enName;
			private BigDecimal available;
			private BigDecimal freez;
			private String unitTag;
			private String key;
			private int unitDecimal;
		}

		@Data
		public static class Base {
			private boolean auth_google_enabled;
			private boolean auth_mobile_enabled;
			private boolean trade_password_enabled;
			private String username;
		}
	}

}