package cc.chenwenxi.zb.rest.domain.resp;

import java.math.BigDecimal;
import java.util.Optional;

import com.google.common.collect.Lists;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import melody.base.entity.type.SideEnum;

/**
 * 订单查询
 *
 * @author lianlianyi@vip.qq.com
 * @date 2017年12月29日 上午11:06:14
 */
@Data
public class OrderBody {
	private String id;
	private BigDecimal price;
	/** 挂单总数量 */
	private BigDecimal total_amount;
	/** 委托时间 */
	private long trade_date;
	/** 挂单状态(1：取消,2：交易完成,3：待成交/待成交未交易部份) */
	private int status;
	/** 已成交总金额 */
	private BigDecimal trade_money;
	/** 已成交数量 */
	private BigDecimal trade_amount;
	/** 挂单类型 1/0[buy/sell] */
	private int type;
	/** 交易类型 */
	private String currency;
	private int acctType;

	public SideEnum getType(){
		if(this.type == 1){
			return SideEnum.buy;
		}
		return SideEnum.sell;
	}

	public Status getStatus() {
		return Status.getEnum(status);
	}

	@Getter
	@AllArgsConstructor
	public enum Status {
		/** 0.待成交 */
		opening(0, "待成交 "),
		/** 3.待成交未交易部分 */
		open(3, "待成交未交易部分"),

		/** 1.取消 */
		cancelled(1, "取消 "),
		/** 2.交易完成 */
		filled(2, "交易完成");


		/** 挂单状态（0、待成交 1、取消 2、交易完成 3、待成交未交易部份） */
		private int key;
		private String cn;

		/** 通过value获取对应的枚举对象 */
		public static Status getEnum(int key) {
			Optional<Status> findFirst = Lists.newArrayList(Status.values()).stream().filter(s -> {
				if (s.getKey() == key) {
					return true;
				}
				return false;
			}).findFirst();
			if (findFirst.isPresent()) {
				return findFirst.get();
			}
			return null;
		}
	}
}
