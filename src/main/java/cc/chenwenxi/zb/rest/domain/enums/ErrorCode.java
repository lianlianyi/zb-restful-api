package cc.chenwenxi.zb.rest.domain.enums;

import java.util.Arrays;

import cc.chenwenxi.zb.rest.exception.ApiException;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorCode {
	ok(1000, "调用成功"),
	e1001(1001, "一般错误提示"),
	e1002(1002, "内部错误"),
	unauthorized(1003, "验证不通过"),
	pwdLock(1004, "资金安全密码锁定"),
	pwdWrong(1005, "资金安全密码错误，请确认后重新输入"),
	e1006(1006, "实名认证等待审核或审核不通过"),
	e1009(1009, "此接口维护中"),
	balanceIsNotEnough(2009, "账户余额不足"),
	invalidPrice(3002, "无效的金额"),
	invalidAmount(3003, "无效的数量"),
	accountNotExistent(3004, "用户不存在"),
	invalidParmas(3005, "无效的参数"),
	invalidIP(3006, "无效的IP或与绑定的IP不一致"),
	invalidTime(3007, "请求时间已失效"),
	invalidTrades(3008, "交易记录没有找到"),
	apiLocked(4001, "API接口被锁定或未启用"),
	tooManyRequests(4002, "请求过于频繁");
    private int code;
    private String message;

	public static ErrorCode fromCode(int code) {
		return Arrays.asList(ErrorCode.values()).stream().filter(f -> f.getCode() == code).findFirst()
				.orElseThrow(() -> new ApiException("找不到错误类型:" + code));
	}
}
