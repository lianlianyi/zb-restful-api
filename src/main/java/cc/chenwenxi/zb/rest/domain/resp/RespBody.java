package cc.chenwenxi.zb.rest.domain.resp;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RespBody<T> {
//    private String status;
//    private long ts;
//    private String ch;
//    private T data;
//    private T tick;
//    private String msg;
    private List<Object> asks;
    private List<Object> bids;
    private Long timestamp;
    
    private String error;


    public String toErrorString() {
        String format = "errMsg:%s.";
        return String.format(format, bids);
    }


}
