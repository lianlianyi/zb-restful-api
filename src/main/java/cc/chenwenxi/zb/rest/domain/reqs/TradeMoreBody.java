package cc.chenwenxi.zb.rest.domain.reqs;
/**
 * 批量下单
 */

import java.math.BigDecimal;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TradeMoreBody {
	private BigDecimal price;
	private BigDecimal amount;
	@Override
	public String toString() {
		return StrUtil.format("[{},{}]", price,amount);
	}
	
	
}
