package cc.chenwenxi.zb.rest.domain.resp;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class TickerBody {
	private Ticker ticker;
	private Long date;

	@Data
	public static class Ticker {
		private String instrument_id;
		private BigDecimal high;
		private BigDecimal low;
		private BigDecimal last;
		private BigDecimal vol;
		private BigDecimal buy;
		private BigDecimal sell;
	}
}