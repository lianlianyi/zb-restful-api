package cc.chenwenxi.zb.rest.domain.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

import lombok.Data;

@Data
//@ConfigurationProperties(prefix = "npc.exchange.zb")
public class ZbApiConfig {
	private String marketUrl;
	private String tradeUrl;
	private String apiKey;
	private String secretKey;
}
