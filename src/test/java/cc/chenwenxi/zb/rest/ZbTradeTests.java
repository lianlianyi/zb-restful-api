package cc.chenwenxi.zb.rest;

import java.io.IOException;

import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cc.chenwenxi.papaya.entity.exchange.Symbol;
import cc.chenwenxi.papaya.entity.exchange.Symbol.Quote;
import cc.chenwenxi.zb.rest.domain.resp.AccountBody;
import cc.chenwenxi.zb.rest.service.trade.TradeApiRestService;
import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ZbTradeTests {

	private TradeApiRestService client;

	private Symbol symbol = new Symbol("ltc", Quote.qc);
	private ZbApiConfig config;
	@Before
	public void config() throws IOException {
//		client = new MarketApiRestServiceImpl();
		config = new ZbApiConfig();
		config.setMarketUrl("http://api.zb.live/data/v1/");
		config.setTradeUrl("https://trade.zb.live/api/");
		config.setApiKey("ea69e6db-b7e2-45fa-a2a2-eec8ae3790f7");
		config.setSecretKey("f6f5a857-8527-4ab4-93b7-fac5d35ffd5b");
	}

	@Test
	public void account() {
		AccountBody account = client.getAccount(config);
		System.out.println(account);
	}

}
