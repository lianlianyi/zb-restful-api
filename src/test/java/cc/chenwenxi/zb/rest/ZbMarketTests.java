package cc.chenwenxi.zb.rest;

import java.io.IOException;
import java.util.List;

import cc.chenwenxi.zb.rest.domain.config.ZbApiConfig;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import cc.chenwenxi.papaya.entity.exchange.Symbol;
import cc.chenwenxi.papaya.entity.exchange.Symbol.Quote;
import cc.chenwenxi.zb.rest.domain.resp.TickerBody;
import cc.chenwenxi.zb.rest.service.market.MarketApiRestService;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import melody.base.entity.res.DeptResult;
import melody.base.entity.res.KlineResult;
import melody.base.entity.type.KTimeEnum;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class ZbMarketTests {

	private MarketApiRestService client;

	private Symbol symbol = new Symbol("ltc", Quote.qc);
	private ZbApiConfig config;
	@Before
	public void config() throws IOException {
//		client = new MarketApiRestServiceImpl();
		config = new ZbApiConfig();
		config.setMarketUrl("http://api.zb.live/data/v1/");
		config.setTradeUrl("https://trade.zb.live/api/");
		config.setApiKey("ea69e6db-b7e2-45fa-a2a2-eec8ae3790f7");
		config.setSecretKey("f6f5a857-8527-4ab4-93b7-fac5d35ffd5b");
	}

	@Test
	public void depth() {
		DeptResult timestamp = client.getDepth(config,symbol,5);
		log.info(JSONUtil.toJsonPrettyStr(timestamp));
	}

//	@Test
	public void kline() {
		List<KlineResult> kline = client.getKline(config,symbol, KTimeEnum.min15, 1000);
		System.out.println(kline);
	}

//	@Test
	public void ticker() {
		TickerBody ticker = client.getTicker(config,symbol);
		System.out.println(ticker);
	}

}
